# Load Libraries ----------------------------------------------------------
library(ggplot2)
library(plotly)
library(flexdashboard)
library(data.table)
library(lubridate)
library(stringr)
library(dplyr)
library(tidyr)
library(purrr) 
library(DT)

# Load Data ------------------------------------------------------------
# Read movies dataset
movies <- read.csv("https://gitlab.com/wqd/movie_popularity/raw/master/data/movies.csv", 
                   encoding = "UTF-8", 
                   colClasses = c("factor","character","factor"))

# Read links dataset
links <- read.csv("https://gitlab.com/wqd/movie_popularity/raw/master/data/links.csv", 
                  encoding = "UTF-8", 
                  colClasses = c("factor","factor","factor"))

# Read ratings dataset
ratings <- read.csv("https://gitlab.com/wqd/movie_popularity/raw/master/data/ratings.csv", 
                    encoding = "UTF-8", 
                    colClasses = c("factor","factor","numeric","numeric"))

# Read tags dataset
tags <- read.csv("https://gitlab.com/wqd/movie_popularity/raw/master/data/tags.csv", 
                 encoding = "UTF-8", 
                 colClasses = c("factor","factor","factor","numeric"))

# Understand Data Structure ---------------------------------------------
# Show R packages
sessionInfo()

# Show Movies structure
head(movies, 5) # show first 5 lines
glimpse(movies) # show summary from dplyr
sapply(movies,function(x) sum(is.na(x)))  # show NA values
sapply(movies,function(x) length(unique(x))) # show unique values
summary(movies) # show summary

# Show Links structure
head(links, 5) # show first 5 lines
glimpse(links) # show summary from dplyr
sapply(links, function(x) sum(is.na(x))) # show NA values
sapply(links, function(x) length(unique(x))) # show unique values
summary(links) # show summary

# Show Ratings structure
head(ratings, 5) # show first 5 lines
glimpse(ratings) # show summary from dplyr
sapply(ratings, function(x) sum(is.na(x))) # show NA values
sapply(ratings, function(x) length(unique(x))) # show unique values
summary(ratings) # show summary

# Show Tags structure
head(tags, 5) # show first 5 lines
glimpse(tags) # show summary from dplyr
sapply(tags, function(x) sum(is.na(x))) # show NA values
sapply(tags, function(x) length(unique(x))) # show unique values
summary(tags) # show summary

# Pre-process Data --------------------------------------------------------
# 1. To combine movies and links as 'main_table'
main_table <- full_join(movies, links, by = "movieId")

# 2. To combine ratings and tags tables as 'user_activity'
user_activity <- full_join(ratings,tags)  %>% 
  # to convert timestamp to date and time
  mutate(timestamp=as_datetime(timestamp))

# 3. To derive movieID_year_era for 'main_table'
movieId_year_era <- movies %>%
  # To trim whitespaces
  mutate(title = str_trim(title)) %>%
  # To get year from title
  mutate(year = str_extract(title, "\\([0-9]{4}\\)")) %>% mutate(year = str_extract(year, "[0-9]{4}")) %>%
  # To get era from year, ignore if NA
  mutate(era = str_sub(year, 1, 3)) %>% mutate(era = ifelse(!is.na(era), paste0(era, "0s"), era)) %>%
  # To select only movieId and year and era
  select(movieId, year, era)

  # To join with 'main_table' and remove unused variable
  main_table <- full_join(main_table, movieId_year_era, by = "movieId")
  rm(movieId_year_era)

# 4. To derive movieId_tags for 'main_table'
movieId_tags <- user_activity %>% filter(!is.na(tag)) %>% select(movieId, tag)  %>% distinct() 

# To identify movie with tag
  movieId_with_tag <- movieId_tags$movieId %>% unique() 
  
  # For each movie with tag get tags in one line separated by "|" 
  movie_tags <- map(movieId_with_tag , function(x){
    # To get tags
    movieId_tags %>% filter(movieId == x) %>% select(tag) %>% pull() %>% 
      # To clean
      str_trim() %>% str_to_lower() %>% 
      # To remove duplicate
      unique() %>% 
      # To collapse into one line
      str_c(collapse = "|")
  }) %>% unlist()
  
  # To combine to get movieId_tags in wide format
  movieId_tags <- cbind(movieId_with_tag, movie_tags) %>% as.data.frame()%>% rename(movieId = movieId_with_tag)
  glimpse(movieId_tags)
  
  # To join with 'main_table' and remove unused variable
  main_table <- full_join(main_table, movieId_tags, by = "movieId")
  rm(movieId_tags, movieId_with_tag, movie_tags)

# 5. To derive movieId_user_summary for 'main_table'
movieId_user_summary <- user_activity %>% select(userId, movieId, rating) %>% group_by(movieId) %>% 
  summarise(user_count = uniqueN(userId), 
            rating_median = median(rating, na.rm = T), 
            rating_average = mean(rating, na.rm = T),
            rating_group = cut(rating_average, breaks = c(0,1,2,3,4,5), labels = c("0.0 to 1.0", "1.0 to 2.0","2.0 to 3.0","3.0 to 4.0","4.0 to 5.0")))

  # To join with 'main_table' and remove unused variable
  main_table <- full_join(main_table, movieId_user_summary, by = "movieId")
  rm(movieId_user_summary)

  
  # To remove variables
  rm(movies)
  rm(links)  
  rm(ratings)
  rm(tags)

# Results and Discussion --------------------------------------------------

###########################################################################
#### View Counts
###########################################################################

# 1. The Most Popular Genres
main_table %>%
  separate_rows(genres, sep = "\\|") %>%
  group_by(genres) %>%
  summarise(total_views = sum(user_count)) %>%
  arrange(desc(total_views))

# 2. The Most Popular Tags
main_table %>%
  separate_rows(movie_tags, sep = "\\|") %>%
  filter(!is.na(movie_tags)) %>% group_by(movie_tags) %>% 
  summarise(total_views = sum(user_count)) %>%
  arrange(desc(total_views)) 

# 3. The Most Popular Rating
main_table %>%
  group_by(rating_group) %>%
  summarise(total_views = sum(user_count)) %>%
  arrange(desc(total_views)) 

# 4. The Most Popular Era
main_table %>%
  group_by(era) %>%
  summarise(total_views = sum(user_count)) %>%
  arrange(desc(total_views)) %>% head(10)

###########################################################################
#### View Counts Per Movies
###########################################################################

# 1. The Most Popular Genres
# To get view counts
table_01 <- main_table %>%
  separate_rows(genres, sep = "\\|") %>%
  group_by(genres) %>%
  summarise(total_views = sum(user_count)) %>%
  arrange(desc(total_views))

# To get movie counts
table_02 <- main_table %>%
  separate_rows(genres, sep = "\\|") %>%
  group_by(genres) %>%
  summarise(total_movies = uniqueN(movieId)) %>%
  arrange(desc(total_movies))

# To calculate View Count Per Movie
full_join(table_01,table_02, by = "genres") %>% mutate(views_per_movie = total_views/total_movies) %>%  
  # To show top 10
  arrange(desc(views_per_movie)) %>% head(10)

# 2. The Most Popular Tags
# To get view counts
table_01 <- main_table %>%
  separate_rows(movie_tags, sep = "\\|") %>%
  filter(!is.na(movie_tags)) %>% group_by(movie_tags) %>% 
  summarise(total_views = sum(user_count)) %>% na.omit() %>% 
  arrange(desc(total_views))

# To get movie counts
table_02 <- main_table %>%
  separate_rows(movie_tags, sep = "\\|") %>%
  filter(!is.na(movie_tags)) %>% group_by(movie_tags) %>% 
  summarise(total_movies = uniqueN(movieId)) %>% na.omit() %>%
  arrange(desc(total_movies))

# To calculate View Count Per Movie
full_join(table_01,table_02, by = "movie_tags") %>% mutate(views_per_movie = total_views/total_movies) %>%  
  # To arrange  
  arrange(desc(views_per_movie)) %>% 
  # To show only tags those apply to at least 10 movies
  filter(total_movies >= 10) %>% 
  # To remove unwanted tags that are less useful
  filter(movie_tags != c("imdb top 250","classic")) %>% 
  # To show top 10
  head(10)

# 3. The Most Popular Rating
# To get view counts
table_01 <- main_table %>%
  filter(!is.na(rating_group)) %>% group_by(rating_group) %>%
  summarise(total_views = sum(user_count)) %>%
  arrange(desc(total_views))

# To get movie counts
table_02 <- main_table %>%
  filter(!is.na(rating_group)) %>% group_by(rating_group) %>%
  summarise(total_movies = uniqueN(movieId)) %>%
  arrange(desc(total_movies))

# To calculate View Count Per Movie
full_join(table_01,table_02, by = "rating_group") %>% mutate(views_per_movie = total_views/total_movies) %>%  
  # To arrange and show top 10
  arrange(desc(views_per_movie)) %>% head(10)

# 4. The Most Popular Era
# To get view counts
table_01 <- main_table %>%
  group_by(era) %>%
  summarise(total_views = sum(user_count)) %>% na.omit() %>% 
  arrange(desc(total_views))

# To get movie counts
table_02 <- main_table %>%
  group_by(era) %>%
  summarise(total_movies = uniqueN(movieId)) %>% na.omit() %>%
  arrange(desc(total_movies))

# To calculate View Count Per Movie
full_join(table_01,table_02, by = "era") %>% mutate(views_per_movie = total_views/total_movies) %>%  
  # To arrange and show top 10
  arrange(desc(views_per_movie)) %>% head(10)


###########################################################################
#### Addressing Changes Over Time 
###########################################################################

# 1. To analyse user activity for movies from different era
table_01 <- user_activity %>% 
  # to convert timestamp to year
  mutate(timestamp_year = year(timestamp)) %>% 
  # to summarise number of views by year
  group_by(movieId, timestamp_year) %>% summarise(user_count = uniqueN(userId)) %>% 
  # to get era for each movieId
  left_join(select(main_table, movieId, era)) %>% 
  # to group by era, timestamp year, to summarise user_count
  group_by(era, timestamp_year) %>% summarise(user_count = sum(user_count)) %>% 
  # to ignore NA era for visualisation
  filter(!is.na(era))

# To visualise 
ggplot(table_01, aes(x = as.integer(timestamp_year), y = user_count, col = era)) + 
  # add point, line, smooth
  geom_point() + geom_line() + geom_smooth() + 
  # facet by era
  facet_wrap(.~era) + 
  # customise axis text, axis title, legend
  theme(axis.text.x = element_text(angle = 90), axis.title = element_blank(), legend.position = "none") + 
  # add title
  ggtitle("Viewers counts changes over time for movies from different era")


# 2. To analyse average number of views by genre by year
# To get view counts
table_01 <- main_table %>%
  separate_rows(genres, sep = "\\|") %>%
  group_by(genres, year) %>%
  summarise(total_views = sum(user_count)) %>%
  arrange(desc(total_views))

# To get movie counts
table_02 <- main_table %>%
  separate_rows(genres, sep = "\\|") %>%
  group_by(genres, year) %>%
  summarise(total_movies = uniqueN(movieId)) %>%
  arrange(desc(total_movies))

# To calculate View Count Per Movie
table_03 <- full_join(table_01,table_02, by = c("genres","year")) %>% mutate(views_per_movie = total_views/total_movies) 
# To get top 10 genres
top10_genres <- table_03 %>% group_by(genres) %>% summarise(views_per_movie = mean(views_per_movie)) %>% top_n(10) %>% select(genres) %>% pull()

# To prepare data for visualisation
table_03  <- table_03 %>% 
  # to focus on top 10 genres only
  filter(genres %in% top10_genres) %>%
  # to focus on year 2001 onwards
  filter(year >= 2001) 

# To visualise 
ggplot(table_03, aes(as.integer(year), views_per_movie))  + 
  # add point, line, smooth
  geom_point() + geom_line() + geom_smooth() + 
  # facet by genres
  facet_wrap(.~genres) + 
  # customise axis text, axis title, legend
  theme(axis.text.x = element_text(angle = 90), axis.title = element_blank()) + 
  # add title
  ggtitle("Average Number of Views From 2001 To 2018 By Genre")

# To remove variables
rm(top10_genres)
rm(table_01, table_02, table_03)

###########################################################################
####  Limiting to New Movie Releases
###########################################################################

# To use only 2013 onwards
main_table_2013 <- main_table %>% filter(year >= 2013)

# 1. The Most Popular Genres
# To get view counts
table_01 <- main_table_2013 %>%
  separate_rows(genres, sep = "\\|") %>%
  group_by(genres) %>%
  summarise(total_views = sum(user_count)) %>%
  arrange(desc(total_views))

# To get movie counts
table_02 <- main_table_2013 %>%
  separate_rows(genres, sep = "\\|") %>%
  group_by(genres) %>%
  summarise(total_movies = uniqueN(movieId)) %>%
  arrange(desc(total_movies))

# To calculate View Count Per Movie
full_join(table_01,table_02, by = "genres") %>% mutate(views_per_movie = total_views/total_movies) %>%  
  # To show top 10
  arrange(desc(views_per_movie)) %>% head(10)

# 2. The Most Popular Tags
# To get view counts
table_01 <- main_table_2013 %>%
  separate_rows(movie_tags, sep = "\\|") %>%
  filter(!is.na(movie_tags)) %>% group_by(movie_tags) %>% 
  summarise(total_views = sum(user_count)) %>% na.omit() %>% 
  arrange(desc(total_views))

# To get movie counts
table_02 <- main_table_2013 %>%
  separate_rows(movie_tags, sep = "\\|") %>%
  filter(!is.na(movie_tags)) %>% group_by(movie_tags) %>% 
  summarise(total_movies = uniqueN(movieId)) %>% na.omit() %>%
  arrange(desc(total_movies))

# To calculate View Count Per Movie
full_join(table_01,table_02, by = "movie_tags") %>% mutate(views_per_movie = total_views/total_movies) %>%  
  # To arrange  
  arrange(desc(views_per_movie)) %>% 
  # To show only tags those apply to at least 3 movies
  filter(total_movies >= 3) %>% 
  # To remove unwanted tags that are less useful
  filter(movie_tags != c("imdb top 250","classic")) %>% 
  # To show top 10
  head(10)

# 3. The Most Popular Rating
# To get view counts
table_01 <- main_table_2013 %>%
  filter(!is.na(rating_group)) %>% group_by(rating_group) %>%
  summarise(total_views = sum(user_count)) %>%
  arrange(desc(total_views))

# To get movie counts
table_02 <- main_table_2013 %>%
  filter(!is.na(rating_group)) %>% group_by(rating_group) %>%
  summarise(total_movies = uniqueN(movieId)) %>%
  arrange(desc(total_movies))

# To calculate View Count Per Movie
full_join(table_01,table_02, by = "rating_group") %>% mutate(views_per_movie = total_views/total_movies) %>%  
  # To arrange and show top 10
  arrange(desc(views_per_movie)) %>% head(10)

# To remove variables
rm(main_table_2013)
rm(table_01, table_02)

###########################################################################
####  Linear Regression Model
###########################################################################

# To use only 2013 onwards
main_table_2013 <- main_table %>% filter(year >= 2013)

# Prepare table
table_01 <- main_table_2013 %>% select(movieId, genres) %>% separate_rows(genres, sep = "\\|") %>% mutate(counta = 1) %>% as.data.table() %>% dcast.data.table(movieId ~ genres, fill = 0) 
table_02 <- main_table_2013 %>% select(movieId, user_count) 
table_03 <- table_01 %>% left_join(table_02, by = "movieId")

# Set model
lm_model <- lm(data = table_03, formula = user_count ~ Action + Adventure + Animation + Children + Comedy + Crime + Documentary + Drama + Fantasy + Horror + IMAX + Musical + Mystery + Romance + `Sci-Fi` + Thriller + War +  Western + 0)

# Result
summary(lm_model)

# To remove variables
rm(main_table_2013)
rm(table_01, table_02, table_03, lm_model)


###########################################################################
####  Analysing Viewers
###########################################################################

# 1. Clustering Viewers

# To use only 2013 onwards
main_table_2013 <- main_table %>% filter(year >= 2013)

# To derive the viewers's activities
# Use data.table package as alternative to dplyr
user_segment_table <- user_activity %>% as.data.table() 
# To filter viewers' activities related to movie released from 2013 onwards, 
# then group by userId, and summarise values for movie_count, mean_rating, and tag_count
user_segment_table <- user_segment_table[movieId %in% main_table_2013$movieId][,.(movie_count = uniqueN(movieId), 
                                                                                  mean_rating = mean(rating, na.rm =  T), 
                                                                                  tag_count = uniqueN(tag)), userId] 

# To remove extreme values for visualisation
user_segment_table <- user_segment_table[movie_count <= quantile(movie_count, 0.95) & movie_count >= quantile(movie_count, 0.05)]
user_segment_table <- user_segment_table[tag_count <= quantile(tag_count, 0.95) & tag_count >= quantile(tag_count, 0.05)]

# To use Kmeans Machine Learning algorithm
segments <- user_segment_table %>% select(movie_count, mean_rating, tag_count) %>% kmeans(centers = 3)

# To assign segments to user_segment_table
user_segment_table[, user_segment := segments$cluster]

# To visualise using plotly
plot_ly(user_segment_table, x = ~movie_count, y = ~mean_rating, z = ~tag_count, color = ~user_segment) %>%
  add_markers() %>%
  layout(scene = list(xaxis = list(title = 'movie_count'),
                      yaxis = list(title = 'mean_rating'),
                      zaxis = list(title = 'tag_count'))) 

# To remove variables
rm(main_table_2013)
rm(segments)
rm(user_segment_table)

# 2. Viewers Preferences
# Total user base in the dataset
user_base <- uniqueN(user_activity$userId)

# % of viewers watched all-time favourite movies
main_table %>%
  group_by(title) %>%
  summarise(total_viewers = sum(user_count), viewer_pct = 100* sum(user_count)/user_base) %>%
  arrange(desc(viewer_pct)) 

# % of viewers watched all-time favourite genres
left_join(select(main_table, movieId, genres), select(user_activity, movieId, userId)) %>% 
  separate_rows(genres, sep = "\\|") %>%
  filter(genres != "(no genres listed)") %>% 
  group_by(genres) %>%
  summarise(total_views = uniqueN(userId), viewer_pct = 100* total_views/user_base) %>%
  arrange(desc(total_views)) 

# % of viewers watched all-time favourite tags
left_join(select(main_table, movieId, movie_tags), select(user_activity, movieId, userId)) %>% 
  separate_rows(movie_tags, sep = "\\|") %>% filter(!is.na(movie_tags)) %>% 
  group_by(movie_tags) %>%
  summarise(total_views = uniqueN(userId), viewer_pct = 100* total_views/user_base) %>%
  arrange(desc(total_views)) 

# To remove variables
rm(user_base)
